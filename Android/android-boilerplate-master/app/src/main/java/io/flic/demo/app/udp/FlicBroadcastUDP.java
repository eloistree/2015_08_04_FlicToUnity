package io.flic.demo.app.udp;

import android.util.Log;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.ServerSocket;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Eloi on 04-08-15.
 */
public class FlicBroadcastUDP {



    private static Thread udpListenerThread;
    private static Thread udpServerSeekerThread;



    public static void StartThreads() {

        udpListenerThread = new Thread(  FlicBroadcastUDP.GetRunnableUDP_ServerListener());
        udpListenerThread.setPriority(Thread.MIN_PRIORITY);
        udpListenerThread.start();
        SeekForServers();

    }
    public static void StopThreads(){
          udpListenerThread.interrupt();
          udpServerSeekerThread.interrupt();
    }

    public static void SeekForServers(){

        if(udpServerSeekerThread!=null)
        udpServerSeekerThread.interrupt();
        udpServerSeekerThread = new Thread(  FlicBroadcastUDP.GetRunnableUDP_SeekForServer());
        udpServerSeekerThread.setPriority(Thread.MIN_PRIORITY);
        udpServerSeekerThread.start();

    }

    private static List<String> serverRegistered = new LinkedList<String>();
    public static boolean HasServers(){
        return serverRegistered.size()>0;
    }
    public static void ResetServerRegister()
    {
        serverRegistered.clear();
        LogServerState();
    }
    public static void AddServerToRegister(String addresse){

        serverRegistered.add(addresse);
        LogServerState();
    }
    private static  void  LogServerState(){
        String serverList="Servers registered:";
        for (String addr : serverRegistered)
            serverList+= " "+addr;
        Log.i("FlicBroadCast",serverList);

    }

    public static  Runnable GetRunnableUDP_SeekForServer(){
        return new Runnable() {
            public void run() {

                    FlicBroadcastUDP.ResetServerRegister();
                    FlicBroadcastUDP.SendSeekForServerMessage();

            }
        };
    }

    public static Runnable GetRunnableUDP_ServerListener(){
        return new Runnable() {


            public void run() {

                DatagramSocket clientSocket=null;
                try{
                    clientSocket = new DatagramSocket(2501);
                    byte[] receiveData = new byte[1024];
                    while(true){

                        Log.i("WhatTheFuck", "Ha ha ha");
                        DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
                        clientSocket.receive(receivePacket);
                        String modifiedSentence = new String(receivePacket.getData());
                        //TODO understant the caracter to remove .... 114 ???
                        String emptyChar = ""+modifiedSentence.charAt(modifiedSentence.length()-1);
                        modifiedSentence= modifiedSentence.replace(emptyChar,"");
                       // Log.i("???", "" + modifiedSentence.charAt(modifiedSentence.length() - 1));
                        Log.i("WhatTheFuck", "FROM SERVER:" + modifiedSentence);
                        if(modifiedSentence.startsWith(("ServerFound:")))
                            FlicBroadcastUDP.AddServerToRegister(receivePacket.getAddress().getHostAddress());
                       // Log.i("WhatTheFuck", "ServerIP:" + receivePacket.getAddress());

                     }
                 }
                catch (Exception e){
                Log.i("FlicBroadCastUPD", "Runnable exception:"+e);
                }
                 finally {
                    if(clientSocket!=null)
                    clientSocket.close();
                }
            }
        };
    }

    public static String [] GetServersRegistered(){
        String [] serversList = new String[serverRegistered.size()];
        serversList = serverRegistered.toArray(serversList);
        return serversList;
    }

    public enum  FlicAction{SingleClick, DoubleClick, LongClick}
    public static void SendButtonState(String buttonId,boolean isDown){


        Log.i("FlicBroadcast", "Button state: " + buttonId+" )> "+isDown);
        SendFlicMessageToServer(buttonId, (isDown?"Down":"Up"), "");


    }
    public static void SendFlicAction(String buttonId, FlicAction action){

        Log.i("FlicBroadcast", "Button action: " + buttonId + " )> " + action);
        SendFlicMessageToServer(buttonId, action.toString(), "");

    }

    public static  void SendFlicMessageToServer(String _buttonId, String _action, String _message){

        _buttonId = _buttonId.replace(":","_");
        _message = _message.replace(":"," ");
        String [] serverList = FlicBroadcastUDP.GetServersRegistered();
        DatagramSocket dsocket=null;
        try{
            dsocket = new DatagramSocket();
            String msg = "FlicAction:"+_buttonId+":"+_action+":"+_message;
            byte [] msgBytes= msg.getBytes();
            for(String addresseTarget : serverList){
                InetAddress newAddress = InetAddress.getByName(addresseTarget);
                Log.i("UDPSEND", "Send to "+addresseTarget+": "+msg);
                DatagramPacket packet = new DatagramPacket(msgBytes,msgBytes.length, newAddress , 2500);
                dsocket.send(packet);
            }


        }catch (Exception e){
            Log.i("UDPSEND",""+e);
        }
        finally {

            if(dsocket!=null)
                dsocket.close();
        }

    }



    private static void SendSeekForServerMessage(){

        DatagramSocket dsocket=null;
        try{
            dsocket = new DatagramSocket();
            Enumeration e = NetworkInterface.getNetworkInterfaces();
            String messageToSend = "SeekServer";
            byte [] messageToSendBytes = messageToSend.getBytes();
            while(e.hasMoreElements())
            {
                NetworkInterface n = (NetworkInterface) e.nextElement();
                Enumeration ee = n.getInetAddresses();
                while (ee.hasMoreElements())
                {
                    InetAddress iAddress = (InetAddress) ee.nextElement();
                    String ipAddress = iAddress.getHostAddress();
                    String [] ipToken = ipAddress.split("\\.");
                    if(ipToken.length==4){
                        Log.i("FlicBroadcast", ">Seek on Ip: " +ipAddress);
                        String pref = ipToken[0]+"."+ipToken[1]+"."+ipToken[2]+".";
                        for (int i = 1; i <= 254; i++) {
                            // Log.i("FlicBroadcast", ">Seek on Ip("+i+"): " +pref+i);
                            InetAddress newAddress = InetAddress.getByName(pref + i);
                            DatagramPacket packet = new DatagramPacket(messageToSendBytes,messageToSendBytes.length, newAddress , 2500);
                            dsocket.send(packet);

                        }
                    }
                }
            }



        }catch (Exception e){
            Log.i("FlicBroadCastUDP","Seek server has exception:"+e);

        }
        finally {

            if(dsocket!=null)
                dsocket.close();
        }

    }

}
