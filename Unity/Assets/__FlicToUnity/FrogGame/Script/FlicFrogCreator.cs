﻿using UnityEngine;
using System.Collections;

public class FlicFrogCreator : MonoBehaviour {

    public GameObject frogPrefab;
    public Transform where;
    public float range = 5f;
   

    void Start()
    {
        Flics.onNewButtonDetected += CreateFrog;

    }
    void OnDestroy()
    {
        Flics.onNewButtonDetected -= CreateFrog;

    }

    private void CreateFrog(Flics.Button button, string id)
    {
        Vector3 whereToCreate = where.position;
        whereToCreate.x+= Random.Range(-range, range);
        GameObject newFrog = GameObject.Instantiate(frogPrefab, whereToCreate, Quaternion.identity) as GameObject;
        FrogFlic frogFlicScript = newFrog.GetComponent<FrogFlic>() as FrogFlic;
        frogFlicScript.SetButtonListened(button);
    }
}
