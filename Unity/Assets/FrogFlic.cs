﻿using UnityEngine;
using System.Collections;

public class FrogFlic : MonoBehaviour {


    public Flics.Button flicButtonListened;
    public Unity2DFrog frog;

    public bool charging;
    public float loadTime = 0;

    public void Update() {
        if (charging){
            if (loadTime == 0)
                NotifyStartLoading();
            loadTime += Time.deltaTime;
        
        }
        else if (loadTime > 0f) {
            if (loadTime > 0.15f )
                NotifyStopLoading(loadTime);
            loadTime = 0f;
        
        }
    }

    private void NotifyStopLoading(float loadTime)
    {

        frog.Jump(loadTime);
    }

    private void NotifyStartLoading()
    {
        loadTime = 0;
    }

    public void SetButtonListened(string id)
    {
        Flics.Button button = Flics.GetButton(id);

    }
    public void SetButtonListened(   Flics.Button button)
    {
        flicButtonListened = button;
        flicButtonListened.onStateChange += ButtonChangedState;
        flicButtonListened.onActionDetected += ButtonActionDetect;
    }

    private void ButtonActionDetect(Flics.Button button, string id, Flics.FlicButtonAction action, string message)
    {
        if (  action == Flics.FlicButtonAction.DoubleClick)
            frog.DoubleJump();
    }

    private void ButtonChangedState(Flics.Button button, string id, bool isDown)
    {
        if (isDown )
            frog.SprintForward();
        charging = isDown;
    }

    public void OnDestroy() { flicButtonListened.onStateChange -= ButtonChangedState; }
}
