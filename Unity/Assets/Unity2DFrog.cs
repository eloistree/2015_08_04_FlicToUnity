﻿using UnityEngine;
using System.Collections;

public class Unity2DFrog : MonoBehaviour {

    public Transform frogHeightDirection;

    public float jumpForce = 10;
    public float doubleJumpForce = 5;
    public float forceFoward = 5;
    public float maxLoadingTime = 1;


    public LineChecker hasGroundChecker;

    public bool hasDoubleJump=true;
    public bool hasSprintFoward=true;

    public void Jump() {
        Jump(1f);
    }

    public void Jump(float loadedTime) {

        if (!hasGroundChecker.IsColliding2D())
            return;
        hasDoubleJump = false; 
        hasSprintFoward = false;
        float powerPct = Mathf.Clamp(loadedTime, 0.3f, maxLoadingTime);
        Rigidbody2D rig2D = gameObject.GetComponent<Rigidbody2D>();
        Vector3 velocity = rig2D.velocity;
        velocity.y += jumpForce * powerPct;
        rig2D.velocity = velocity;
 //       gameObject.GetComponent<Rigidbody2D>().AddForce(frogHeightDirection.up * jumpForce*powerPct, ForceMode2D.Impulse);
    }


    public void DoubleJump() {

        if (hasGroundChecker.IsColliding2D())
            return;
        if (hasDoubleJump)
            return;
        hasDoubleJump = true;

        Rigidbody2D rig2D = gameObject.GetComponent<Rigidbody2D>();
        Vector3 velocity = rig2D.velocity;

        if (velocity.y < 0) velocity.y = 0;

        velocity.y += velocity.y<=0? doubleJumpForce : doubleJumpForce/2f;
       
        rig2D.velocity = velocity;
    }


    public void SprintForward()
    {
        if (hasGroundChecker.IsColliding2D())
            return;

        if (hasSprintFoward)
            return;
        Rigidbody2D rig2D = gameObject.GetComponent<Rigidbody2D>();
        hasSprintFoward = true;

        Vector3 velocity = rig2D.velocity;

        velocity.y = 0.3f*forceFoward;
        velocity.x = forceFoward;

        
        rig2D.velocity = velocity;
 
        
    
    }


    public void OnCollisionEnter2D(Collision2D col)
    {
        int groundLayer =LayerMask.NameToLayer("Ground");
        if (col.gameObject.layer == groundLayer)
        {
            Rigidbody2D rig2D = gameObject.GetComponent<Rigidbody2D>();
            rig2D.velocity = Vector3.zero;
        }
            


    }
}
