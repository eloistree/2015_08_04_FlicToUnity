﻿using UnityEngine;
using System.Collections.Generic;

public class UDPToButtonsState : MonoBehaviour {

    public UDP_Receiver receiver;
    public Queue<string> messageReceived = new Queue<string>();

    public const string onClick = "Click";
    public const string onDoubleClick = "DoubleClick";
    public const string onPressed = "Pressed";

    public virtual void Start()
    {
        StartListenToReceiver();
    }

    public virtual void Update()
    {

        while (messageReceived.Count >= 1)
            UpdateButtonState(messageReceived.Dequeue());
    }
    private void StartListenToReceiver()
    {
        receiver.onPackageReceivedParallelThread += ReceivedUdpMessage;

    }

    public void OnDestroy()
    {
        receiver.onPackageReceivedParallelThread -= ReceivedUdpMessage;

    }
    private void ReceivedUdpMessage(UDP_Receiver from, string _message, string _ip, int _port)
    {
        if (_message.IndexOf("FlicAction:") != 0) return;
        messageReceived.Enqueue(_message);
    }


    protected virtual void UpdateButtonState(string udpMessage)
    {

        string[] tokens = udpMessage.Split(':');
        if (tokens.Length != 4) return;

        string buttonId = tokens[1];
        string actionType = tokens[2];
        string message = tokens[3];




    }

}
