﻿using UnityEngine;
using System.Collections;

public class Fly : MonoBehaviour {

    public Transform restrictedZoneCenter;
    public float radiusVertical=3f;
    public float radiusHorizontal=10f;
    public Vector3 whereToGo;
    public float speed = 2f;
    public float minTimeChangeDirection=0.5f;
    public float maxTimeChangeDirection=3f;


	void Start () {
        ChangeDestination();
        StartCoroutine(ChangeWhereToGo());
	}

    private IEnumerator ChangeWhereToGo()
    {
        float timeNextChange = maxTimeChangeDirection;

        while (true) {
            yield return new WaitForSeconds(timeNextChange);
            timeNextChange = Random.Range(minTimeChangeDirection, maxTimeChangeDirection);
            ChangeDestination();
        }
    }

    private void ChangeDestination()
    {
        whereToGo.x = restrictedZoneCenter.position.x + Random.Range(-radiusHorizontal, radiusHorizontal);
        whereToGo.y = restrictedZoneCenter.position.y + Random.Range(-radiusVertical, radiusVertical);
    }
	
	// Update is called once per frame
	void Update () {
        transform.position = Vector3.MoveTowards(transform.position, whereToGo, Time.deltaTime*speed);
        if((transform.position-whereToGo).magnitude<0.1f)
            ChangeDestination();
	}

    public void TeleportNewDestination() {
        ChangeDestination();
        transform.position = whereToGo;
    }


    public void OnTriggerEnter2D(Collider2D col){
        TeleportNewDestination();

    }
}
