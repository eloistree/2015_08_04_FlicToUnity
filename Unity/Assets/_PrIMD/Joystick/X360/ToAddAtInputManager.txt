
  - serializedVersion: 3
    m_Name: X axis
    descriptiveName: 
    descriptiveNegativeName: 
    negativeButton: 
    positiveButton: 
    altNegativeButton: 
    altPositiveButton: 
    gravity: 0
    dead: .189999998
    sensitivity: 1
    snap: 0
    invert: 0
    type: 2
    axis: 0
    joyNum: 0
  - serializedVersion: 3
    m_Name: Y axis
    descriptiveName: 
    descriptiveNegativeName: 
    negativeButton: 
    positiveButton: 
    altNegativeButton: 
    altPositiveButton: 
    gravity: 0
    dead: .189999998
    sensitivity: 1
    snap: 0
    invert: 1
    type: 2
    axis: 1
    joyNum: 0
  - serializedVersion: 3
    m_Name: 5th axis
    descriptiveName: 
    descriptiveNegativeName: 
    negativeButton: 
    positiveButton: 
    altNegativeButton: 
    altPositiveButton: 
    gravity: 0
    dead: .189999998
    sensitivity: 1
    snap: 0
    invert: 1
    type: 2
    axis: 4
    joyNum: 0
  - serializedVersion: 3
    m_Name: 4th axis
    descriptiveName: 
    descriptiveNegativeName: 
    negativeButton: 
    positiveButton: 
    altNegativeButton: 
    altPositiveButton: 
    gravity: 0
    dead: .189999998
    sensitivity: 1
    snap: 0
    invert: 0
    type: 2
    axis: 3
    joyNum: 0
  - serializedVersion: 3
    m_Name: 6th axis
    descriptiveName: 
    descriptiveNegativeName: 
    negativeButton: 
    positiveButton: 
    altNegativeButton: 
    altPositiveButton: 
    gravity: 0
    dead: .189999998
    sensitivity: 1
    snap: 0
    invert: 0
    type: 2
    axis: 5
    joyNum: 0
  - serializedVersion: 3
    m_Name: 7th axis
    descriptiveName: 
    descriptiveNegativeName: 
    negativeButton: 
    positiveButton: 
    altNegativeButton: 
    altPositiveButton: 
    gravity: 0
    dead: .189999998
    sensitivity: 1
    snap: 0
    invert: 0
    type: 2
    axis: 6
    joyNum: 0
  - serializedVersion: 3
    m_Name: 3rd axis triggers
    descriptiveName: 
    descriptiveNegativeName: 
    negativeButton: 
    positiveButton: 
    altNegativeButton: 
    altPositiveButton: 
    gravity: 0
    dead: .189999998
    sensitivity: 1
    snap: 0
    invert: 1
    type: 2
    axis: 2
    joyNum: 0
  - serializedVersion: 3
    m_Name: 9rd axis left trigger
    descriptiveName: 
    descriptiveNegativeName: 
    negativeButton: 
    positiveButton: 
    altNegativeButton: 
    altPositiveButton: 
    gravity: 0
    dead: .189999998
    sensitivity: 1
    snap: 0
    invert: 0
    type: 2
    axis: 8
    joyNum: 0
  - serializedVersion: 3
    m_Name: 10rd axis right trigger
    descriptiveName: 
    descriptiveNegativeName: 
    negativeButton: 
    positiveButton: 
    altNegativeButton: 
    altPositiveButton: 
    gravity: 0
    dead: .189999998
    sensitivity: 1
    snap: 0
    invert: 0
    type: 2
    axis: 9
    joyNum: 0
  - serializedVersion: 3
    m_Name: P1 X axis
    descriptiveName: 
    descriptiveNegativeName: 
    negativeButton: 
    positiveButton: 
    altNegativeButton: 
    altPositiveButton: 
    gravity: 0
    dead: .189999998
    sensitivity: 1
    snap: 0
    invert: 0
    type: 2
    axis: 0
    joyNum: 1
  - serializedVersion: 3
    m_Name: P1 Y axis
    descriptiveName: 
    descriptiveNegativeName: 
    negativeButton: 
    positiveButton: 
    altNegativeButton: 
    altPositiveButton: 
    gravity: 0
    dead: .189999998
    sensitivity: 1
    snap: 0
    invert: 1
    type: 2
    axis: 1
    joyNum: 1
  - serializedVersion: 3
    m_Name: P1 5th axis
    descriptiveName: 
    descriptiveNegativeName: 
    negativeButton: 
    positiveButton: 
    altNegativeButton: 
    altPositiveButton: 
    gravity: 0
    dead: .189999998
    sensitivity: 1
    snap: 0
    invert: 1
    type: 2
    axis: 4
    joyNum: 1
  - serializedVersion: 3
    m_Name: P1 4th axis
    descriptiveName: 
    descriptiveNegativeName: 
    negativeButton: 
    positiveButton: 
    altNegativeButton: 
    altPositiveButton: 
    gravity: 0
    dead: .189999998
    sensitivity: 1
    snap: 0
    invert: 0
    type: 2
    axis: 3
    joyNum: 1
  - serializedVersion: 3
    m_Name: P1 6th axis
    descriptiveName: 
    descriptiveNegativeName: 
    negativeButton: 
    positiveButton: 
    altNegativeButton: 
    altPositiveButton: 
    gravity: 0
    dead: .189999998
    sensitivity: 1
    snap: 0
    invert: 0
    type: 2
    axis: 5
    joyNum: 1
  - serializedVersion: 3
    m_Name: P1 7th axis
    descriptiveName: 
    descriptiveNegativeName: 
    negativeButton: 
    positiveButton: 
    altNegativeButton: 
    altPositiveButton: 
    gravity: 0
    dead: .189999998
    sensitivity: 1
    snap: 0
    invert: 0
    type: 2
    axis: 6
    joyNum: 1
  - serializedVersion: 3
    m_Name: P1 3rd axis triggers
    descriptiveName: 
    descriptiveNegativeName: 
    negativeButton: 
    positiveButton: 
    altNegativeButton: 
    altPositiveButton: 
    gravity: 0
    dead: .189999998
    sensitivity: 1
    snap: 0
    invert: 1
    type: 2
    axis: 2
    joyNum: 1
  - serializedVersion: 3
    m_Name: P1 9rd axis left trigger
    descriptiveName: 
    descriptiveNegativeName: 
    negativeButton: 
    positiveButton: 
    altNegativeButton: 
    altPositiveButton: 
    gravity: 0
    dead: .189999998
    sensitivity: 1
    snap: 0
    invert: 0
    type: 2
    axis: 8
    joyNum: 1
  - serializedVersion: 3
    m_Name: P1 10rd axis right trigger
    descriptiveName: 
    descriptiveNegativeName: 
    negativeButton: 
    positiveButton: 
    altNegativeButton: 
    altPositiveButton: 
    gravity: 0
    dead: .189999998
    sensitivity: 1
    snap: 0
    invert: 0
    type: 2
    axis: 9
    joyNum: 1
  - serializedVersion: 3
    m_Name: P2 X axis
    descriptiveName: 
    descriptiveNegativeName: 
    negativeButton: 
    positiveButton: 
    altNegativeButton: 
    altPositiveButton: 
    gravity: 0
    dead: .189999998
    sensitivity: 1
    snap: 0
    invert: 0
    type: 2
    axis: 0
    joyNum: 2
  - serializedVersion: 3
    m_Name: P2 Y axis
    descriptiveName: 
    descriptiveNegativeName: 
    negativeButton: 
    positiveButton: 
    altNegativeButton: 
    altPositiveButton: 
    gravity: 0
    dead: .189999998
    sensitivity: 1
    snap: 0
    invert: 1
    type: 2
    axis: 1
    joyNum: 2
  - serializedVersion: 3
    m_Name: P2 5th axis
    descriptiveName: 
    descriptiveNegativeName: 
    negativeButton: 
    positiveButton: 
    altNegativeButton: 
    altPositiveButton: 
    gravity: 0
    dead: .189999998
    sensitivity: 1
    snap: 0
    invert: 1
    type: 2
    axis: 4
    joyNum: 2
  - serializedVersion: 3
    m_Name: P2 4th axis
    descriptiveName: 
    descriptiveNegativeName: 
    negativeButton: 
    positiveButton: 
    altNegativeButton: 
    altPositiveButton: 
    gravity: 0
    dead: .189999998
    sensitivity: 1
    snap: 0
    invert: 0
    type: 2
    axis: 3
    joyNum: 2
  - serializedVersion: 3
    m_Name: P2 6th axis
    descriptiveName: 
    descriptiveNegativeName: 
    negativeButton: 
    positiveButton: 
    altNegativeButton: 
    altPositiveButton: 
    gravity: 0
    dead: .189999998
    sensitivity: 1
    snap: 0
    invert: 0
    type: 2
    axis: 5
    joyNum: 2
  - serializedVersion: 3
    m_Name: P2 7th axis
    descriptiveName: 
    descriptiveNegativeName: 
    negativeButton: 
    positiveButton: 
    altNegativeButton: 
    altPositiveButton: 
    gravity: 0
    dead: .189999998
    sensitivity: 1
    snap: 0
    invert: 0
    type: 2
    axis: 6
    joyNum: 2
  - serializedVersion: 3
    m_Name: P2 3rd axis triggers
    descriptiveName: 
    descriptiveNegativeName: 
    negativeButton: 
    positiveButton: 
    altNegativeButton: 
    altPositiveButton: 
    gravity: 0
    dead: .189999998
    sensitivity: 1
    snap: 0
    invert: 1
    type: 2
    axis: 2
    joyNum: 2
  - serializedVersion: 3
    m_Name: P2 9rd axis left trigger
    descriptiveName: 
    descriptiveNegativeName: 
    negativeButton: 
    positiveButton: 
    altNegativeButton: 
    altPositiveButton: 
    gravity: 0
    dead: .189999998
    sensitivity: 1
    snap: 0
    invert: 0
    type: 2
    axis: 8
    joyNum: 2
  - serializedVersion: 3
    m_Name: P2 10rd axis right trigger
    descriptiveName: 
    descriptiveNegativeName: 
    negativeButton: 
    positiveButton: 
    altNegativeButton: 
    altPositiveButton: 
    gravity: 0
    dead: .189999998
    sensitivity: 1
    snap: 0
    invert: 0
    type: 2
    axis: 9
    joyNum: 2
  - serializedVersion: 3
    m_Name: P3 X axis
    descriptiveName: 
    descriptiveNegativeName: 
    negativeButton: 
    positiveButton: 
    altNegativeButton: 
    altPositiveButton: 
    gravity: 0
    dead: .189999998
    sensitivity: 1
    snap: 0
    invert: 0
    type: 2
    axis: 0
    joyNum: 3
  - serializedVersion: 3
    m_Name: P3 Y axis
    descriptiveName: 
    descriptiveNegativeName: 
    negativeButton: 
    positiveButton: 
    altNegativeButton: 
    altPositiveButton: 
    gravity: 0
    dead: .189999998
    sensitivity: 1
    snap: 0
    invert: 1
    type: 2
    axis: 1
    joyNum: 3
  - serializedVersion: 3
    m_Name: P3 5th axis
    descriptiveName: 
    descriptiveNegativeName: 
    negativeButton: 
    positiveButton: 
    altNegativeButton: 
    altPositiveButton: 
    gravity: 0
    dead: .189999998
    sensitivity: 1
    snap: 0
    invert: 1
    type: 2
    axis: 4
    joyNum: 3
  - serializedVersion: 3
    m_Name: P3 4th axis
    descriptiveName: 
    descriptiveNegativeName: 
    negativeButton: 
    positiveButton: 
    altNegativeButton: 
    altPositiveButton: 
    gravity: 0
    dead: .189999998
    sensitivity: 1
    snap: 0
    invert: 0
    type: 2
    axis: 3
    joyNum: 3
  - serializedVersion: 3
    m_Name: P3 6th axis
    descriptiveName: 
    descriptiveNegativeName: 
    negativeButton: 
    positiveButton: 
    altNegativeButton: 
    altPositiveButton: 
    gravity: 0
    dead: .189999998
    sensitivity: 1
    snap: 0
    invert: 0
    type: 2
    axis: 5
    joyNum: 3
  - serializedVersion: 3
    m_Name: P3 7th axis
    descriptiveName: 
    descriptiveNegativeName: 
    negativeButton: 
    positiveButton: 
    altNegativeButton: 
    altPositiveButton: 
    gravity: 0
    dead: .189999998
    sensitivity: 1
    snap: 0
    invert: 0
    type: 2
    axis: 6
    joyNum: 3
  - serializedVersion: 3
    m_Name: P3 3rd axis triggers
    descriptiveName: 
    descriptiveNegativeName: 
    negativeButton: 
    positiveButton: 
    altNegativeButton: 
    altPositiveButton: 
    gravity: 0
    dead: .189999998
    sensitivity: 1
    snap: 0
    invert: 1
    type: 2
    axis: 2
    joyNum: 3
  - serializedVersion: 3
    m_Name: P3 9rd axis left trigger
    descriptiveName: 
    descriptiveNegativeName: 
    negativeButton: 
    positiveButton: 
    altNegativeButton: 
    altPositiveButton: 
    gravity: 0
    dead: .189999998
    sensitivity: 1
    snap: 0
    invert: 0
    type: 2
    axis: 8
    joyNum: 3
  - serializedVersion: 3
    m_Name: P3 10rd axis right trigger
    descriptiveName: 
    descriptiveNegativeName: 
    negativeButton: 
    positiveButton: 
    altNegativeButton: 
    altPositiveButton: 
    gravity: 0
    dead: .189999998
    sensitivity: 1
    snap: 0
    invert: 0
    type: 2
    axis: 9
    joyNum: 3
  - serializedVersion: 3
    m_Name: P4 X axis
    descriptiveName: 
    descriptiveNegativeName: 
    negativeButton: 
    positiveButton: 
    altNegativeButton: 
    altPositiveButton: 
    gravity: 0
    dead: .189999998
    sensitivity: 1
    snap: 0
    invert: 0
    type: 2
    axis: 0
    joyNum: 4
  - serializedVersion: 3
    m_Name: P4 Y axis
    descriptiveName: 
    descriptiveNegativeName: 
    negativeButton: 
    positiveButton: 
    altNegativeButton: 
    altPositiveButton: 
    gravity: 0
    dead: .189999998
    sensitivity: 1
    snap: 0
    invert: 1
    type: 2
    axis: 1
    joyNum: 4
  - serializedVersion: 3
    m_Name: P4 5th axis
    descriptiveName: 
    descriptiveNegativeName: 
    negativeButton: 
    positiveButton: 
    altNegativeButton: 
    altPositiveButton: 
    gravity: 0
    dead: .189999998
    sensitivity: 1
    snap: 0
    invert: 1
    type: 2
    axis: 4
    joyNum: 4
  - serializedVersion: 3
    m_Name: P4 4th axis
    descriptiveName: 
    descriptiveNegativeName: 
    negativeButton: 
    positiveButton: 
    altNegativeButton: 
    altPositiveButton: 
    gravity: 0
    dead: .189999998
    sensitivity: 1
    snap: 0
    invert: 0
    type: 2
    axis: 3
    joyNum: 4
  - serializedVersion: 3
    m_Name: P4 6th axis
    descriptiveName: 
    descriptiveNegativeName: 
    negativeButton: 
    positiveButton: 
    altNegativeButton: 
    altPositiveButton: 
    gravity: 0
    dead: .189999998
    sensitivity: 1
    snap: 0
    invert: 0
    type: 2
    axis: 5
    joyNum: 4
  - serializedVersion: 3
    m_Name: P4 7th axis
    descriptiveName: 
    descriptiveNegativeName: 
    negativeButton: 
    positiveButton: 
    altNegativeButton: 
    altPositiveButton: 
    gravity: 0
    dead: .189999998
    sensitivity: 1
    snap: 0
    invert: 0
    type: 2
    axis: 6
    joyNum: 4
  - serializedVersion: 3
    m_Name: P4 3rd axis triggers
    descriptiveName: 
    descriptiveNegativeName: 
    negativeButton: 
    positiveButton: 
    altNegativeButton: 
    altPositiveButton: 
    gravity: 0
    dead: .189999998
    sensitivity: 1
    snap: 0
    invert: 1
    type: 2
    axis: 2
    joyNum: 4
  - serializedVersion: 3
    m_Name: P4 9rd axis left trigger
    descriptiveName: 
    descriptiveNegativeName: 
    negativeButton: 
    positiveButton: 
    altNegativeButton: 
    altPositiveButton: 
    gravity: 0
    dead: .189999998
    sensitivity: 1
    snap: 0
    invert: 0
    type: 2
    axis: 8
    joyNum: 4
  - serializedVersion: 3
    m_Name: P4 10rd axis right trigger
    descriptiveName: 
    descriptiveNegativeName: 
    negativeButton: 
    positiveButton: 
    altNegativeButton: 
    altPositiveButton: 
    gravity: 0
    dead: .189999998
    sensitivity: 1
    snap: 0
    invert: 0
    type: 2
    axis: 9
    joyNum: 4